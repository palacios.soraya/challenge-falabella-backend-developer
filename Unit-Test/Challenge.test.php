<?php

  require 'challenge.php';
  use PHPUnit\Framework\TestCase;

  class ChallengeTest extends TestCase {
      public function test_challengeTest_returns_value_as_expected() {
      $expect = '1<br>2<br>Falabella<br>4<br>IT<br>Falabella<br>7<br>8<br>Falabella<br>IT<br>11<br>Falabella<br>13<br>14<br>Integraciones<br>16<br>17<br>Falabella<br>19<br>IT<br>Falabella<br>22<br>23<br>Falabella<br>IT<br>26<br>Falabella<br>28<br>29<br>Integraciones<br>31<br>32<br>Falabella<br>34<br>IT<br>Falabella<br>37<br>38<br>Falabella<br>IT<br>41<br>Falabella<br>43<br>44<br>Integraciones<br>46<br>47<br>Falabella<br>49<br>IT<br>Falabella<br>52<br>53<br>Falabella<br>IT<br>56<br>Falabella<br>58<br>59<br>Integraciones<br>61<br>62<br>Falabella<br>64<br>IT<br>Falabella<br>67<br>68<br>Falabella<br>IT<br>71<br>Falabella<br>73<br>74<br>Integraciones<br>76<br>77<br>Falabella<br>79<br>IT<br>Falabella<br>82<br>83<br>Falabella<br>IT<br>86<br>Falabella<br>88<br>89<br>Integraciones<br>91<br>92<br>Falabella<br>94<br>IT<br>Falabella<br>97<br>98<br>Falabella<br>IT<br>';
      $this->assertEquals($expect, print_numbers_change_multiples(1,100));
    }
  }
