<?php
 /**
*
* Challenge Falabella Backend Developer
* 
* Write a program that prints all the numbers from 1 to 100. However, for
* multiples of 3, instead of the number, print "Falabella". For multiples of 5 print
* "IT". For numbers which are multiples of both 3 and 5, print "Integraciones".
* But here's the catch: you can use only one `if`. No multiple branches, ternary
* operators or `else`.
*
* @author Soraya Palacios
* @author https://www.linkedin.com/in/palacios-soraya/
*
* 
*/

/**
* The function print_numbers_change_multiples: return an string with all numbers from $start to $end. For multiples of 3 instead of the number write "Falabella", for multiples of 5 write "IT" 
* and for multiples of both 3 and 5 write "Integraciones". 
* @param $start is the first number to the secuence for write
* @param $end is the last number to the secuence for write
*/

function print_numbers_change_multiples($start, $end) {
    $all = [];
    $text = ["Falabella", "IT", "Integraciones"]; 
    
    for ($i = $start; $i <= $end; $i++) {
        array_push($all, array((is_multiples($i, 3) == 0), (is_multiples($i, 5) == 0), ((is_multiples($i, 5) == 0) && (is_multiples($i, 3) == 0))));
    }
    $show = ""; 
    foreach ($all as $key => $value) {
        $show1 = $key + 1;
        foreach ($value as $key1 => $val) {
            if ($val == 1)
                $show1 = $text[$key1];
        }
        $show .= $show1 . "<br>";
    }
    return $show;
}

/**
* The function is_multiples return an boolean if $i is multiples of $n
* 
* @param type $i number to compare with $n
* @param type $n multiples number
* @return type
* 
*/

function is_multiples($i, $n) {
    return $i % $n;
}

/**
 * Shows on the Browser results of print_numbers_change_multiples with start in 1 and end in 100
 */

echo "<pre>";
echo print_numbers_change_multiples(1,100);
echo "<pre>";

?>
